Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: clap
Upstream-Contact: Marek Marecki <marekjm@ozro.pw>
Source: https://github.com/marekjm/clap

Files: Changelog.markdown
       clap/builder.py
       clap/checker.py
       clap/errors.py
       clap/formatter.py
       clap/helper.py
       clap/__init__.py
       clap/mode.py
       clap/option.py
       clap/parser.py
       clap/shared.py
       DESIGN.markdown
       Makefile
       README.markdown
       setup.py
       tests/clap/buildertests.py
       tests/clap/tests.py
       tox.ini
       .gitignore
Copyright: 2012-2020, Marek Marecki <marekjm@ozro.pw>
License: GPL-3+ or LGPL-3+
Comment:
 Using GPL-3+ as found in LICENSE file and LGPL-3+ as found in
 README.markdown file. Upstream said via issue opened in its repository
 to ignore MIT quoted in setup.py file.

Files: debian/*
Copyright: 2020, Paulo Henrique de Lima Santana (phls) <phls@debian.org>
           2022, Franklin Timoteo dos Santos <franklindev@disroot.org>
License: GPL-3+ or LGPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".
